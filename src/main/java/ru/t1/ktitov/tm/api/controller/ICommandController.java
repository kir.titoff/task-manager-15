package ru.t1.ktitov.tm.api.controller;

public interface ICommandController {

    void showInfo();

    void showWelcome();

    void showVersion();

    void showAbout();

    void showArguments();

    void showCommands();

    void showHelp();

}
