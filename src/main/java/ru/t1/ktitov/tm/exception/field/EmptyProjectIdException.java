package ru.t1.ktitov.tm.exception.field;

public class EmptyProjectIdException extends AbstractFieldException {

    public EmptyProjectIdException() {
        super("Error! Project id is empty.");
    }

}
