package ru.t1.ktitov.tm.exception.field;

public class EmptyIdException extends AbstractFieldException {

    public EmptyIdException() {
        super("Error! Id is empty.");
    }

}
