package ru.t1.ktitov.tm.exception.system;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Not supported argument.");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error! Not supported argument: " + argument + ".");
    }

}
