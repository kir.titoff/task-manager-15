package ru.t1.ktitov.tm.exception.system;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Not supported command.");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Not supported command: " + command + ".");
    }

}
