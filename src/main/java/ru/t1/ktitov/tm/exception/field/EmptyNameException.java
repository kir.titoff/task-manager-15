package ru.t1.ktitov.tm.exception.field;

public class EmptyNameException extends AbstractFieldException {

    public EmptyNameException() {
        super("Error! Name is empty.");
    }

}
