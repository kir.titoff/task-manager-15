package ru.t1.ktitov.tm.exception.field;

public class EmptyTaskIdException extends AbstractFieldException {

    public EmptyTaskIdException() {
        super("Error! Task id is empty.");
    }

}
