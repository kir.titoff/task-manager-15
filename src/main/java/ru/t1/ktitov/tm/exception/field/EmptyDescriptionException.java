package ru.t1.ktitov.tm.exception.field;

public class EmptyDescriptionException extends AbstractFieldException {

    public EmptyDescriptionException() {
        super("Error! Description is empty.");
    }

}
